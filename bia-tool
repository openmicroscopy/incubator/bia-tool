#!/usr/bin/env python

import errno
import logging
import os
import sys
import socket
import time
logging.basicConfig()
os.environ["OMERO_SESSIONDIR"] = "/tmp/sessions"
sys.path.insert(0, "/opt/omero/server/OMERO.server/lib/python")

from omero.cli import cli_login, NonZeroReturnCode
from omero.gateway import BlitzGateway
from omero.util.import_candidates import as_dictionary
from argparse import ArgumentParser


def wait_on_omero():
    end = time.time() + 30
    s = socket.socket()
    while True:
        try:
            next_timeout = end - time.time()
            if next_timeout < 0:
                return False
            else:
                s.settimeout(next_timeout)
                s.connect(("omero", 4064))
        except socket.timeout, err:
            return False
        except socket.error, err:
            if type(err.args) != tuple:
                raise
            elif err[0] not in (errno.ECONNABORTED, errno.ECONNREFUSED, errno.ETIMEDOUT):
                raise
        else:
            s.close()
            return True


def list_files(input_dir, output_dir):
    count = 0
    for k, v in as_dictionary(input_dir).items():
        count += 1
        dir = os.path.join(output_dir, "out_%d" % count)
        key = os.path.join(dir, "main_file.txt")
        list = os.path.join(dir, "used_files.txt")
        os.makedirs(dir)
        with open(key, "w") as o:
            print >>o, k
        with open(list, "w") as o:
            for file in v:
                print >>o, file
        yield k, dir


def generate_thumb(cli, file, output_dir):
    # TODO: this code is similar to fsDropBoxMonitoryClient
    stderr = os.path.join(output_dir, "stderr")
    stdout = os.path.join(output_dir, "stdout")
    try:
        cli.onecmd(["import", "--transfer=ln_s",
                    "---errs=%s" % stderr,
                    "---file=%s" % stdout,
                    "--checksum-algorithm=File-Size-64",
                    "--parallel-upload=10",
                    file])
        cli.assertRC()  # TODO: onecmd doesn't take "strict" kw
        images = []
        with open(stdout, "r") as o:
            lines = o.readlines()
            for line in lines:
                if line.startswith("Image:"):
                    images.append(long(line[6:-1]))

        conn = BlitzGateway(client_obj=cli.get_client())
        for image_id in images:
            image = conn.getObject("Image", image_id)
            jpeg_data = image.getThumbnail()
            with open(os.path.join(output_dir, "%s.jpg" % image_id), "w") as o:
                o.write(jpeg_data)
    except NonZeroReturnCode:
        print "Failed to import (TODO: write log)"


if __name__ == "__main__":
    parser = ArgumentParser("bia-tool")
    parser.add_argument("input_dir")
    parser.add_argument("output_dir")
    ns = parser.parse_args()
    wait_on_omero()
    for retry in range(9, 0, -1):
        try:
            time.sleep(1)
            # TODO: retry could be a part of this contract
            with cli_login("root@omero", "-w", "omero") as cli:
                for file, dir in list_files(ns.input_dir, ns.output_dir):
                    generate_thumb(cli, file, dir)
                break
        except Exception, e:
            if "Failed to login" not in e.message:
                raise
            elif retry == 0:
                print "Failed to login"
                raise
            else:
                print "Login retries left... (%s)" % retry

FROM openmicroscopy/omero-server:5.4
USER root
RUN pip install omero-cli-render
# TODO: could be an option on omero-server-docker
RUN mkdir -p --mode=u+rwxs,g+rwxs,o+rwxs /OMERO/ManagedRepository/root_0
RUN sed -i -e 's/set -eu/set -eu\numask a+rwx/' /startup/99-run.sh
USER omero-server
COPY bia-tool /usr/local/bin/

ID ?= $(shell id -u)
IN ?= $(PWD)/in
OUT ?= $(PWD)/out
export ID

build:
	docker-compose build

$(IN):
	mkdir -p $(IN)

$(OUT):
	mkdir -p $(OUT)

$(IN)/a.fake: $(IN)
	touch $(IN)/a.fake

$(IN)/b.fake: $(IN)
	touch $(IN)/b.fake

run: $(IN)/a.fake $(IN)/b.fake $(OUT) build
	./up

clean:
	rm -rf $(IN) $(OUT)

.PHONY: build run clean
